#!/usr/bin/python3
# -*- coding: utf-8 -*-
import computecount


def csv(fichero: str):
    with open(fichero, 'numeros') as cuenta:  # Abro el fichero para ver los parametros escritos en el mismo
        texto = cuenta.read().split()
        cont = 0
        operacion = computecount.Compute()
        for ln in texto:
            ln_actual = ln.split(' ')
            if cont == 0:
                try:
                    dft = float(ln_actual[0])
                except ValueError:
                    print(
                        'Bad format')  # En caso de no encontrar o encontrar un valor menor o igual a 0 el programa
                    # devolverá un mensaje de error
                else:
                    if dft <= 0:
                        raise ValueError('El valor por defecto debe ser mayor de 0')
                    else:
                        operacion.default = float(
                            ln_actual[0])  # En primer lugar se realiza el calculo con la variable por defecto
                        print(f'Default: {operacion.default}')
            else:
                calculo = ln_actual[0]
                if calculo != 'power' and calculo != 'log':  # Si al leer el fichero se encuentra un intento de
                    # llamar a una operacion que nmo sea ni power ni log, devuelve un mensaje de error
                    print('Bad format')
                else:
                    try:
                        n = float(ln_actual[1])
                    except ValueError:
                        print('Bad format')
                    else:
                        if len(ln_actual) == 2:  # En caso correcto dependiendo de la operacion pedida, llamará a la
                            # función necesaria
                            if calculo == 'log':
                                print(operacion.log(n, operacion.default))
                            elif calculo == 'power':
                                print(operacion.power(n, operacion.default))
                        elif len(ln_actual) == 3:  # Si encuentra un segundo numero empieza a realizar el calculo pedido
                            try:
                                n2 = float(ln_actual[2])
                            except ValueError:
                                print('Bad format')
                            else:
                                if calculo == 'log':
                                    print(operacion.log(n, n2))
                                elif calculo == 'power':
                                    print(operacion.power(n, n2))
                        elif len(ln_actual) > 3:
                            print('Bad format')
            cont += 1
        print(f'Operations: {operacion.count}')  # Llamada a la función para contar el número de operaciones hechas
    cuenta.close()


if __name__ == 'main':
    csv('operations.csv')  # Llamada a la función para realizar el cálculo sobre el fichero operations.csv
    
    # No conozco la causa del mal funcionamiento del programa, lo he testeado antes y funcionaba correctamente,
    # a simple vista no parece que haya modificado nada en el mismo pero aguna linea se habrá modificado para que
    # deje de funcionar como debe
