#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Compute two math functions: raise to the power and logarithm.

The main code of the module takes operands and operation (raise or log)
from the command line, as arguments when running the program.

The format for calling the module from the command line is:
compute.py <operation> <num> [<num2>]
<num2> (exponent for power, base for log) is optional: it is
assumed to be 2 if not specified.

For example:
compute.py power 2 3
8.0
compute.py log 8 2
3.0
"""

import math


class Compute:  # Esta clase simplemente se basa en utilizar count en cada proceso para contar el numero de veces que
    # se realiza cada uno
    count = 0

    def __init__(self):
        self.default = 2

    def set_def(self, base):
        self.count += 1
        if base is None:
            return self.default
        else:
            if base <= 0:
                raise ValueError('No se acepta una base menor o igual a 0')
            else:
                self.default = base
                return base

    def get_def(self):
        self.count += 1
        return self.default

    def power(self, n1, exp=2):
        """ Function to compute number to the exp power"""
        self.count += 1
        return n1 ** exp

    def log(self, n1, base=2):
        """ Function to compute log base of number"""
        self.count += 1
        return math.log(n1, base)
