#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Compute two math functions: raise to the power and logarithm.

The main code of the module takes operands and operation (raise or log)
from the command line, as arguments when running the program.

The format for calling the module from the command line is:
compute.py <operation> <num> [<num2>]
<num2> (exponent for power, base for log) is optional: it is
assumed to be 2 if not specified.

For example:
compute.py power 2 3
8.0
compute.py log 8 2
3.0
"""

import math
import sys


class Compute:

    def __init__(self, n0=2):
        self.default = n0

    def power(self, n1, exp):
        """ Function to compute number to the exp power"""
        sol = n1 ** exp
        return sol

    def log(self, n1, base):
        """ Function to compute log base of number"""
        sol = math.log(n1, base)
        return sol


if __name__ == "__main__":
    calculo = Compute(2)
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = calculo.default
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = calculo.power(num, num2)
    elif sys.argv[1] == "log":
        result = calculo.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)
